select st.staff_id,st.first_name ||' '|| st.last_name, st.address_id ,st.email ,st.active ,st.username ,st."password"  , sum(pay.amount) as revenue,st.store_id 
	from staff  as st
	join payment as pay 
		on st.staff_id =pay.staff_id 
	where extract (year from pay.payment_date) = 2017
	group by st.staff_id 
	order by revenue desc; 

select st.staff_id, st.first_name || ' ' || st.last_name, st.address_id, st.email, st.active, st.username, st."password", revenue, st.store_id
	from staff as st
	join ( select staff_id,sum(amount) as revenue
    from payment
	where extract (year from payment.payment_date) = 2017
    group by staff_id
) as pay
on st.staff_id = pay.staff_id
order by revenue desc;

select f.film_id, f.title, count(r.rental_id) as rental_count, f.rating 
	from film as f
	join inventory as i
		on f.film_id = i.film_id
	join rental as r
		on i.inventory_id = r.inventory_id
	group by f.film_id, f.title
	order by rental_count desc
		limit 5;
   
select f.title, f.rating
	from film as f
	where f.film_id in (
        select film_id
        from (
            select f.film_id, count(r.rental_id) as rental_count
            from film as f
            join inventory as i
            on f.film_id = i.film_id
            join rental as r
            on i.inventory_id = r.inventory_id
            group by f.film_id
            order by rental_count desc
            limit 5 
           ) as subquery );
 
  select  a.actor_id,a.first_name ||' '|| a.last_name as actor, max(f.release_year) as latest_film, 2023-max(f.release_year) as is_not_acting_for_years
  	from actor as a
  	join film_actor as fa 
  		on a.actor_id =fa.actor_id
  	join film f
  		on f.film_id = fa.film_id 
  	group by a.actor_id 
  	order by latest_film asc ;

	select actor_id, first_name || ' ' || last_name, film_count, last_update
	from (
    	select a.actor_id,a.first_name || ' ' || a.last_name,COUNT(fa.film_id) as film_count, a.last_update
    	from actor as a
    	join film_actor as fa
    		on a.actor_id = fa.actor_id
    	group by a.actor_id, a.first_name, a.last_name, a.last_update
	) as subquery
	order by film_count desc;

select a.actor_id, a.actor, max(f.release_year) as latest_film, 2023 - max(f.release_year) as is_not_acting_for_years
from (
    select actor_id, first_name || ' ' || last_name as actor
    from actor
) as a
join film_actor as fa on a.actor_id = fa.actor_id
join film as f on f.film_id = fa.film_id
group by a.actor_id, a.actor
order by latest_film desc;
  
   

